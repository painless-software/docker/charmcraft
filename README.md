# Charmcraft

The Charmcraft CLI in a container for CI automation.

## Usage

```yaml
charmcraft:
  image: registry.gitlab.com/painless-software/docker/charmcraft:2.6.0
  script:
  - charmcraft pack --destructive-mode
  artifacts:
    paths:
    - '*.charm'
```

## Tags

- version tag ... stable images with Charmcraft version `major.minor.patch`
- `latest` ... latest stable image (tagged with a Charmcraft version)
- `main` ... edge image built from the `main` branch
- `mr<IID>` ... review images built from merge requests, which are deleted
   once a MR is merged or closed

See the repository's [container registry][registry] for all image tags.

## Development

See [CONTRIBUTING][contrib] for instructions on development and testing.

## Related Resources

- [Charmcraft source code][repo] (GitHub)
- [Charmcraft package][pypi] (PyPI)

[contrib]: CONTRIBUTING.md
[pypi]: https://pypi.org/project/charmcraft
[registry]: https://gitlab.com/painless-software/docker/charmcraft/container_registry
[repo]: https://github.com/canonical/charmcraft
