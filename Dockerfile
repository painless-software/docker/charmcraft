# Install charmcraft as a Python package. (It's hard to install a snap in a container image.)
# https://github.com/canonical/charmcraft

ARG DOCKER_REGISTRY=docker.io

FROM ${DOCKER_REGISTRY}/library/ubuntu:22.04

ARG DEBIAN_FRONTEND=noninteractive
ARG VERSION
ARG PYTHONAPT
ARG USER=charmcraft
ARG HOME=/home/${USER}

ENV PATH=$HOME/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin \
    PIP_DISABLE_PIP_VERSION_CHECK=1 \
    PIP_NO_CACHE_DIR=1

RUN apt-get update \
 && apt-get upgrade -y \
 && apt-get install -y \
    curl \
    git \
    libapt-pkg-dev \
    libffi-dev \
    libssl-dev \
    python-is-python3 \
    python3 \
    python3-pip \
    python3-venv \
 && apt-get clean \
 && useradd -u 1001 -U ${USER} -m \
 && pip install https://launchpad.net/ubuntu/+archive/primary/+sourcefiles/python-apt/${PYTHONAPT}/python-apt_${PYTHONAPT}.tar.xz \
 && pip install git+https://github.com/canonical/charmcraft@${VERSION} \
# Support arbitrary user IDs (OpenShift guidelines)
 && chown -R ${USER}:0 ${HOME} /var/lib/apt/lists \
 && chmod -R g=u       ${HOME} /var/lib/apt/lists

WORKDIR ${HOME}

USER ${USER}
