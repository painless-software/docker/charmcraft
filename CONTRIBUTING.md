# Contributing

You're welcome to contribute updates, bug fixes or other enhancements!

## Development

1. Look up the current latest version number in the charmcraft
   [GitHub repository][repo] (and on [PyPI][pypi]).
1. Update the `VERSION` number in the [CI configuration][version]. Verify
   [`requirements-jammy.txt`][requirements-jammy] in the source code
   repository to see whether you need to update the `PYTHONAPT` version.
1. Commit and push changes to a topic branch and open a MR to build a
   review image.
1. Wait for the MR to be merged, which will build the *edge* image based
   on the `main` branch.
1. To create a *stable* image [create a release with a Git tag][releases]
   matching the Charmcraft version.

The CI/CD pipeline will automatically build container images with
various tags as described in the [README][tags]. See the repository's
[container registry][registry] for all current image tags.

## Tests

[Functional tests and vulnerability checks][tests] run in the CI/CD
pipeline's `test` stage using the freshly built container image.
When you add features or fix a bug, please add or update a related test.

While a MR is active you'll find an image tagged `mr<IID>`, the merge
request ID, in the [container registry][registry] for your convenience,
which you can use to manually test-drive your changes.

[repo]: https://github.com/canonical/charmcraft/releases
[pypi]: https://pypi.org/project/charmcraft/#history
[releases]: https://gitlab.com/painless-software/docker/charmcraft/-/releases
[registry]: https://gitlab.com/painless-software/docker/charmcraft/container_registry
[requirements-jammy]: https://github.com/canonical/charmcraft/blob/main/requirements-jammy.txt
[version]: .gitlab-ci.yml#L3-4
[tests]: .gitlab-ci.yml#L97-129
[tags]: README.md#tags
